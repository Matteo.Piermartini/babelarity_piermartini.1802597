package it.uniroma1.lcl.babelarity;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/**
 * Definisce il metodo per calcolare la similarit� tra due documenti
 */
public class BabelDocumentSimilarityAvanzata 
{
	/**
	 * La mappa isA contiene come chiave  synset e come valore tutti i synset che sono i relazione "is-a" con il 
	 * synset che � posto come chiave. 
	 */
	private static Map<Synset, Set<Synset>> isA = BabelSemanticSimilarityAvanzata.isA;
	/**
	 * La mappa hasKind contiene come chiave  synset e come valore tutti i synset che sono i relazione "has-kind" con il 
	 * synset che � posto come chiave. 
	 */
	private static Map<Synset, Set<Synset>> hasKind = BabelSemanticSimilarityAvanzata.hasKind;
	/** 
	 * la mappa altreRelazioni contiene tutte quei synset che non sono in relazione n� con isA e n� con haskind.
	 */
	private static Map<Synset, Set<Synset>> altreRelazioni = BabelSemanticSimilarityAvanzata.altreRelazioni;
	private static Map<Synset,Set<Synset>> mappa = new HashMap<>();	
	/**
	 * 
	 * Nel costruttore della document creo la mappa con tutti i synset che sono in relazione 
	 * tra di loro
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public BabelDocumentSimilarityAvanzata() throws FileNotFoundException, IOException {creaMappa();}
	public static void creaMappa()
	{
		for(Synset synset : isA.keySet())
		{
			mappa.putIfAbsent(synset, isA.get(synset));
			if(mappa.keySet().contains(synset)) 
			{
				Set<Synset> sett = mappa.get(synset);
				sett.addAll(isA.get(synset));
				mappa.replace(synset, sett);
			}
		}
		for(Synset synset : hasKind.keySet())
		{
			mappa.putIfAbsent(synset, hasKind.get(synset));
			if(mappa.keySet().contains(synset)) 
			{
				Set<Synset> sett = mappa.get(synset);
				sett.addAll(hasKind.get(synset));
				mappa.replace(synset, sett);
			}
		}
		for(Synset synset : altreRelazioni.keySet())
		{
			mappa.putIfAbsent(synset, altreRelazioni.get(synset));
			if(mappa.keySet().contains(synset)) 
			{
				Set<Synset> sett = mappa.get(synset);
				sett.addAll(altreRelazioni.get(synset));
				mappa.replace(synset, sett);
			}
		}
	}
	/**
	 * Il grafo semantico di un documento � composto dai synset estratti dalle parole del testo connessi tra loro 
	 * tramite le relazioni definite in miniBabelNet.
	 * @param document
	 * @throws IOException
	 */
	public Map<Synset,Set<Synset>> grafo(Document document) throws IOException
	{
		Map<Synset,Set<Synset>> grafo = new HashMap<>();
		MiniBabelNet miniBabelNet = new MiniBabelNet();
		String contenuto = document.getContent().toLowerCase().replace("\\W", " ");
		String[] parole = contenuto.split(" ");
		List<Synset> synsetsInDocumento1 = new ArrayList<>();
		for(String string : parole)
		{
			List<String> lemmi = MiniBabelNet.getLemmas(string);
			for(String s : lemmi) 
				synsetsInDocumento1.addAll(miniBabelNet.getSynsets(s));
		}			
		Set<Synset> synsetsInDocumento = new HashSet<>(synsetsInDocumento1);
		for(Synset synset : synsetsInDocumento) grafo.put(synset, new HashSet<>());
		for(Synset synset : synsetsInDocumento)
		{
			if(mappa.containsKey(synset))
			{
				for(Synset synset2 : mappa.get(synset))
				{
					if(synsetsInDocumento.contains(synset2))
					{
						grafo.putIfAbsent(synset, new HashSet<>());
						grafo.putIfAbsent(synset2, new HashSet<>());
						if(grafo.keySet().contains(synset2))
						{
							Set<Synset> set= new HashSet<>();
							set.addAll(grafo.get(synset2));
							set.add(synset);
							grafo.replace(synset2, set);
						}
						if(grafo.keySet().contains(synset))
						{
							Set<Synset> set= new HashSet<>();
							set.addAll(grafo.get(synset));
							set.add(synset2);
							grafo.replace(synset, set);
						}
					}
				}
			}
		}
		for(Synset synset : synsetsInDocumento)
		{
			if(!grafo.containsKey(synset))
			{
				if(mappa.containsKey(synset))
				{   
					for(Synset synset2 : mappa.get(synset))
					{
						for(Synset synset3 : mappa.get(synset2))
						{
							if(synsetsInDocumento.contains(synset3))
							{
								grafo.putIfAbsent(synset, new HashSet<>());
								grafo.putIfAbsent(synset3, new HashSet<>());
								if(grafo.keySet().contains(synset3))
								{
									Set<Synset> set= new HashSet<>();
									set.addAll(grafo.get(synset3));
									set.add(synset);
									grafo.replace(synset3, set);
								}

								if(grafo.keySet().contains(synset))
								{
									Set<Synset> set= new HashSet<>();
									set.addAll(grafo.get(synset));
									set.add(synset3);
									grafo.replace(synset, set);
								}
							}
						}
					}
				}
			}
		}
		for(Synset synset : grafo.keySet())
		{
			if(grafo.get(synset).isEmpty())
			{
				Set<Synset> set = new HashSet<>();
				set.add(synset);
				grafo.replace(synset, set);
			}
		}
		return grafo;
	}
	public Map<Synset,Integer> randomWalk(Map<Synset,Set<Synset>> grafo)
	{
		Map<Synset,Integer> visite = new HashMap<>();
		for(Synset b: grafo.keySet())
			visite.put(b, 0);
		int maxVisit = 100000;
		Random r = new Random();
		Synset root = new ArrayList<>(visite.keySet()).get(r.nextInt(visite.size()));
		while(maxVisit>0)
		{
			if(Math.random()>0.1)root = new ArrayList<>(visite.keySet()).get(r.nextInt(visite.size()));
			visite.put(root, visite.get(root)+1);
			int random = r.nextInt(grafo.get(root).size());
			root = new ArrayList<>(grafo.get(root)).get(random);
			maxVisit--;
		}
		return visite;
	}
	/**
	 * La similarit� del coseno, o cosine similarity, � una tecnica euristica per la misurazione della similitudine tra due vettori
	 * effettuata calcolando il coseno tra di loro, usata generalmente per il confronto di testi nel data 
	 * mining e nell'analisi del testo 
	 * @param vettore1
	 * @param vettore2
	 * @return
	 */
	private double cosineSimilarity(Map<Synset, Integer> vettore1, Map<Synset, Integer> vettore2) 
	{
		double numeratore = 0;
		double denominatore = 0;
		double v3 =0;
		double v4 =0;
		for(Synset s : vettore1.keySet())
		{
				double v1 =  vettore1.get(s);	
				double v2 = (double) vettore2.get(s);
				v3 += Math.pow((double) vettore1.get(s),2);
				v4 += Math.pow((double) vettore2.get(s),2);
				numeratore += v1 * v2;
		}
		v3 = Math.sqrt(v3);
		v4 = Math.sqrt(v4);
		denominatore = v3 * v4;
		return numeratore / denominatore;
	}
	/**
	 * in computeSimilarity richiamo tutti i metodi necessari per il calcolo della similarit�
	 * tra due documenti
	 * @param o1
	 * @param o2
	 * @return
	 * @throws IOException
	 */
	public double computeSimilarity(LinguisticObject o1, LinguisticObject o2) throws IOException
	{
		Document d1 = (Document)o1;
		Document d2 = (Document)o2;
		Map<Synset,Set<Synset>> grafo = grafo(d1);
		Map<Synset,Set<Synset>> grafo2 = grafo(d2);
		Map<Synset,Integer> vettore1 = randomWalk(grafo);
		Map<Synset,Integer> vettore2 = randomWalk(grafo2);
		for(Synset s :vettore1.keySet()) if(!vettore2.containsKey(s))vettore2.put(s, 0);
		for(Synset s :vettore2.keySet()) if(!vettore1.containsKey(s))vettore1.put(s, 0);
		return cosineSimilarity(vettore1,vettore2);
	}
	
	
	
	
}