package it.uniroma1.lcl.babelarity;


import java.util.HashMap;
import java.util.Map;
/**
 * Il calcolo della similarit� lessicale tra due parole utilizza la rete semantica miniBabelNet contando il numero di parole in 
 * comune tra le definizioni di tutti i synset in cui appare il lemma della prima parola e quelli della seconda. 
 * @author user
 *
 */
public class BabelLexicalSimilarity 
{ 
	/**
	 * La mappa ListaTesti contiene come stringa il nome del file e come Set le parole FILTRATE DI CIASCUN DOCUMENTO
	 * In creaVettore si crea il vettore della parola data in input.
	 * Ritrona il valore della formula del pmi.
	 * @param x
	 * @return
	 */
	public static CaricaFile listaTesti = new CaricaFile();
	/**
	 * creaVettor crea il vettore per il calcolo della similarit�.
	 * Ogni parola w � rappresentata attraverso un vettore di co-occorrenza, ovvero un istogramma lungo |V|,
	 * nel quale il valore della componente i-esima indica quanto la parola w tende ad apparire in un testo 
	 * insieme alla parola i-esima nel vocabolario V.
	 * @param x
	 * @return
	 */
	private Map<String, Double> creaVettore(String x) 
	{
		Map<String, Double> vettore = new HashMap<>();
		double numeroDocumenti = CaricaFile.getNumeroDocumenti();
		for(String y : listaTesti.getListaTesti().keySet())
		{
			double freqx = listaTesti.getListaTesti().get(x).size();
			double freqy = listaTesti.getListaTesti().get(y).size();
			double freqxy = 0;
			for(String s : listaTesti.getListaTesti().get(y)) if(listaTesti.getListaTesti().get(x).contains(s)) freqxy +=1;
			double divisione = (((freqxy ) /numeroDocumenti) /  
					(( (freqx / numeroDocumenti) * ((freqy) / numeroDocumenti))));
			if (divisione <= 0) vettore.put(y, 0.0);
			else vettore.put(y,Math.log(divisione));
		}
		return vettore;
	}
	/**
	 * Il calcolo del cosineSimilarity, uguale per tutte e tre le similarit�.
	 * La similarit� del coseno, o cosine similarity, � una tecnica euristica per la misurazione della similitudine tra due vettori
	 * effettuata calcolando il coseno tra di loro, usata generalmente per il confronto di testi nel data 
	 * mining e nell'analisi del testo.
	 * @param vettore1
	 * @param vettore2
	 * @return
	 */
	private double cosineSimilarity(Map<String, Double> vettore1, Map<String, Double> vettore2) 
	{
		double numeratore = 0;
		double denominatore = 0;
		double v3 =0;
		double v4 =0;
		for(String s : listaTesti.getListaTesti().keySet())
		{
				double v1 = (double) vettore1.get(s);	
				double v2 = (double) vettore2.get(s);
				v3 += Math.pow((double) vettore1.get(s),2);
				v4 += Math.pow((double) vettore2.get(s),2);
				numeratore += v1 * v2;	
		}
		v3 = Math.sqrt(v3);
		v4 = Math.sqrt(v4);
		denominatore = v3 * v4;
		return numeratore / denominatore;
	}
	/**
	 * in computeSimilarity richiamo tutti i metodi necessari per il calcolo della similarit�
	 * tra due word                                                                                                                                                                                                
	 * @param o1
	 * @param o2
	 * @return
	 */
	public double computeSimilarity(LinguisticObject o1 , LinguisticObject o2)
	{
		Word w1 = (Word) o1;
		Word w2 = (Word) o2;
		Map<String, Double> vettore1 = creaVettore(w1.getString());
		Map<String, Double> vettore2 = creaVettore(w2.getString());
		if(vettore1.equals(vettore2)) return 1.0;
		return cosineSimilarity(vettore1, vettore2);
	}
	
	
	
	
}
