package it.uniroma1.lcl.babelarity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
/**
 * E' la classe che implementa l'algoritmo base della similart� semantica.
 * Viee utulizzata questa classe perch� nell'algoritmo della similarit� avanzata c'�
 * bisogno dell'algoritmo base quando non viene ricavato nessun risultato da quella avanzata.
 * @author user
 *
 */
public class BabelSemanticSimilarity 
{
	public static Map<Synset, ArrayList<Synset>> nodiDaCercare = MiniBabelNet.relazioni;
	/**
	 * Attraverso il BFS controllo tutti i figli cio� tutti i synsets che sono in relazione
	 * "has-kind".
	 * @param o1
	 * @param o2
	 * @return
	 */
	public double BFS (LinguisticObject o3, LinguisticObject o4)
	{
		Synset o1 = (Synset) o3;
		Synset o2 = (Synset) o4;
		o1.setContatore(0);
		LinkedList<Synset>  coda = new LinkedList<>();//CODA
		HashSet<Synset> visitati = new HashSet<>();
		coda.add(o1);
		visitati.add(o1);
		while(!coda.isEmpty())
		{
			Synset padre = coda.remove(0);
			if(padre.equals(o2)) return padre.getContatore();
			if(nodiDaCercare.containsKey(padre))
			{	
				for(Synset figli : getSynset(padre))
				{
					if(!visitati.contains(figli))
					{
						figli.setContatore(padre.getContatore()+1);
						visitati.add(figli);
						coda.add(figli);
					}
				}
			}
		}
		return 0;
	}
	public List<Synset> getSynset(Synset input) {return nodiDaCercare.get(input);}
}

