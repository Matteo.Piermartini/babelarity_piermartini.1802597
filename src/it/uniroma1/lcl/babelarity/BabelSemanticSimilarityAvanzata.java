package it.uniroma1.lcl.babelarity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * La similarit� semantica � definita tra due synset e sfrutta le informazioni della rete semantica miniBabelNet: 
 * la rete definisce le relazioni semantiche di vario tipo tra i vari concetti ed entit� 
 * @author user
 *
 */
public class BabelSemanticSimilarityAvanzata 
{
	/**
	 * isA � una mappa che contiene tutti come chiave un synset e come valore un set
	 * contenente tutti i synset che sono in relazione "isA" con il synset chiave.
	 */
	public static Map<Synset,Set<Synset>> isA = new HashMap<>();
	/**
	 * hasKind � una mappa che contiene tutti come chiave un synset e come valore un set
	 * contenente tutti i synset che sono in relazione "hasKind" con il synset chiave.
	 */
	public static Map<Synset,Set<Synset>> hasKind= new HashMap<>();
	/**
	 * altreRelazioni � una mappa che contiene tutti come chiave un synset e come valore un set
	 * contenente tutti i synset che non sono in relazione "isA" e "hasKind" con il synset chiave.
	 */
	public static Map<Synset,Set<Synset>> altreRelazioni= new HashMap<>();
	private static final String FILE_PATH_RELATIONS = "resources/relations.txt";
	private static double lunghezzaGrafo;
	/**
	 * Il costruttore richiama il metodo per la creazione delle mappe che sono campi attraverso
	 * il metodo "relazioniFile()".
	 * Calcola la lunghezza del grafo salvando il valore nella variabile "lunghezzaGrafo".
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public BabelSemanticSimilarityAvanzata() throws FileNotFoundException, IOException 
	{
		relazioniFile();
		lunghezzaGrafo();
	}
	/**
	 * relazioniFile() legge il file relations.txt e salva i corrispettivi synsets nelle mappe.
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void relazioniFile() throws FileNotFoundException, IOException 
	{
		try(BufferedReader br = new BufferedReader(new FileReader(new File(FILE_PATH_RELATIONS))))
		{
			while(br.ready())
			{
				String linea = br.readLine();
				String[] parole = linea.split("\t");
				
				Set<Synset> sett = new HashSet<>();
				sett.add(new Synset (parole[0]));
				
				if(parole[2].equals("is-a"))
				{
					Synset s = new Synset(parole[1]);
					isA.putIfAbsent(s, sett);
					Synset nuovo = new Synset(parole[0]);
					if(!isA.get(s).contains(nuovo)) 
					{
						Set<Synset> sett2 = new HashSet<>();
						sett2.add(nuovo);
						sett2.addAll(isA.get(s));
						sett.addAll(sett2);
						isA.replace(s, sett);
					}
				}
				if(parole[2].equals("has-kind"))
				{
					Synset s = new Synset(parole[1]);
					hasKind.putIfAbsent(s, sett);
					Synset nuovo = new Synset(parole[0]);
					if(!hasKind.get(s).contains(nuovo)) 
					{
						Set<Synset> sett2 = new HashSet<>();
						sett2.add(nuovo);
						sett2.addAll(hasKind.get(s));
						sett.addAll(sett2);
						hasKind.replace(s, sett);
					}
				}
				if(parole[2].equals("part-of") || parole[2].equals("has-part"))
				{
					Synset s = new Synset(parole[1]);
					altreRelazioni.putIfAbsent(s, sett);
					Synset nuovo = new Synset(parole[0]);
					if(!altreRelazioni.get(s).contains(nuovo)) 
					{
						Set<Synset> sett2 = new HashSet<>();
						sett2.add(nuovo);
						sett2.addAll(altreRelazioni.get(s));
						sett.addAll(sett2);
						altreRelazioni.replace(s, sett);
					}
				}
			}
		}
	}
	/**
	 * Visitando le mappe, con l'algoritmo BFS si ricava la lunghezza massima del grafo.
	 * 
	 */
	public static void lunghezzaGrafo()
	{
		List<Synset> tuttiSynset = new ArrayList<>();
		tuttiSynset.addAll(isA.keySet());
		List<Integer> lunghezze = new ArrayList<>();
		Set<Synset> nodi = new HashSet<>();
		for(Synset s : tuttiSynset)
			{
				LinkedList<Synset>  coda = new LinkedList<>();
				Set<Synset> padriVisitati = new HashSet<>();
				s.setContatore(0);
				coda.add(s);
				while(!coda.isEmpty())
				{
					Synset padre = coda.remove(0);
					padriVisitati.add(padre);
					if(isA.containsKey(padre))
					{	
						for(Synset figlio : isA.get(padre))
						{
							if(!padriVisitati.contains(figlio))
							{
								figlio.setContatore(padre.getContatore()+1);
								padriVisitati.add(figlio);
								coda.add(figlio);
							}
						}
					}
					else nodi.add(padre);
				}
			}
		for(Synset s : nodi)
		{
			LinkedList<Synset>  coda = new LinkedList<>();
			Set<Synset> padriVisitati = new HashSet<>();
			coda.add(s);
			while(!coda.isEmpty())
			{
				Synset padre = coda.remove(0);
				padriVisitati.add(padre);
				if(hasKind.containsKey(padre))
				{	
					for(Synset figlio : hasKind.get(padre))
					{
						if(!padriVisitati.contains(figlio))
						{
							figlio.setContatore(padre.getContatore()+1);
							padriVisitati.add(figlio);
							coda.add(figlio);
						}
					}
				}
				else lunghezze.add(padre.contatore);
			}
		}
		lunghezzaGrafo = Collections.max(lunghezze);
	}
	/**
	 * L�algoritmo LCH consiste nell�individuazione del Least Common Subsumer (LCS) di due concetti A e B, 
	 * ovvero il concetto pi� specifico che sia antenato sia di A che B. 
	 * Il calcolo dell�LHC consiste in una semplice visita dell�albero �a ritroso�, partendo dal nodo sorgente attraversando solamente archi �is-a�
	 * fino alla radice. Una volta calcolati i cammini nodo-radice di entrambi i synset si consideri (se esiste) il nodo a livello nell�intersezione 
	 * dei due cammini che si colloca a livello pi� basso nella tassonomia.
	 * @param primo
	 * @param destinazione
	 * @return
	 */
	public double lch(LinguisticObject o1, LinguisticObject o2)
	{
		Synset s1 = (Synset) o1;
		Synset s2 = (Synset) o2;
		
		double  valore = lhc(s1,s2);
		if (valore != Integer.MAX_VALUE) 
			{
				if(valore == 0)	return 1.0;
				double logaritmo = valore / (2*lunghezzaGrafo);
				return -Math.log(logaritmo);
			}
		else return 1 / (new BabelSemanticSimilarity().BFS(s1, s2)) +1;
	}
	public double lhc(LinguisticObject s1, LinguisticObject s2)
	{
		Synset o1 = (Synset) s1;
		Synset o2 = (Synset) s2;
		LinkedList<Synset>  coda = new LinkedList<>();
		Set<Synset> padriVisitati = new HashSet<>();
		o1.setContatore(0);
		coda.add(o1);
		while(!coda.isEmpty())
		{
			Synset padre = coda.remove(0);
			padriVisitati.add(padre);
			if(padre.equals(o2)) return padre.getContatore();
			double ris =BFS(padre,o2);
			if(ris != Integer.MAX_VALUE) return ris;
			if(isA.containsKey(padre))
			{
				for(Synset padreDelPadre : isA.get(padre))
				{
					if(!padriVisitati.contains(padreDelPadre))
					{
						padreDelPadre.setContatore(padre.getContatore()+1);
						padriVisitati.add(padreDelPadre);
						coda.add(padreDelPadre);
					}
				}
			}
			else
			{
				double bfs = BFS(padre,o2);
				if(bfs != Integer.MAX_VALUE){return bfs;}
				else return Integer.MAX_VALUE;
			}
		}
		return Integer.MAX_VALUE;
	}
	/**
	 * Attraverso il BFS controllo tutti i figli cio� tutti i synsets che sono in relazione
	 * "has-kind".
	 * @param o1
	 * @param o2
	 * @return
	 */
	public double BFS (Synset o1, Synset o2)
	{
		LinkedList<Synset>  coda = new LinkedList<>();
		HashSet<Synset> visitati = new HashSet<>();
		coda.add(o1);
		while(!coda.isEmpty())
		{
			Synset padre = coda.remove(0);
			visitati.add(padre);
			if(padre.equals(o2)) return padre.contatore;
			if(hasKind.containsKey(padre))
			{	
				for(Synset figlio : hasKind.get(padre))
				{
					if(!visitati.contains(figlio))
					{
						figlio.setContatore(padre.getContatore()+1);
						visitati.add(figlio);
						coda.add(figlio);
					}
				}
			}
		}
		return Integer.MAX_VALUE;
	}
}
