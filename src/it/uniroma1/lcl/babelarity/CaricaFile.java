package it.uniroma1.lcl.babelarity;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * La classe CaricaFile legge tutti i file necessari per lo sviluppo della similarit� lessicale.
 * @author user
 *
 */
public class CaricaFile 
{
	public static Map<String,String> lemmi = MiniBabelNet.getLemmi();
	/**
	 * listaTesti � il vocabolario di tutte le parole, cio� i lemmi, di tutte le parole lette nei file
	 * che si trovano della cartella corpus.
	 * E' una mappa che come chiave ha il nome dei file e come valore un set che contiene tutte le parole filtrate 
	 * di quel documento.
	 */
	public static Map<String, HashSet<String>> listaTesti = new HashMap<>();
	/**
	 * E' il numero di documenti presenti nella cartella corpus.
	 */
	public static double numeroDocumenti;
	/**
	 * Il costruttore di CaricaFile richiama i metodi per la creazione delle stopWords e crea il vocabolario.
	 */
	public CaricaFile(){creaVocabolario();}
	public static Set<String> stopWord = new HashSet<>();
	/**
	 * La mappa contiene come stringa il nome del file e come Set le parole FILTRATE DI CIASCUN DOCUMENTO.
	 */
	public static void creaVocabolario() 
	{
		stopWords();
		File folder = new File("resources/corpus/");
		File[] listOfFiles = folder.listFiles();
	   for (File f: listOfFiles)
	   {
		   String documento = "";
		   numeroDocumenti += 1;
		   try
		   {
			   List<String> righe = Files.readAllLines(f.toPath());
			   for (String s: righe)
				   documento += s.toLowerCase().replaceAll("\\W", " ");
			   String[] paroleSplittate = documento.split(" ");
			   Set<String> set = new HashSet<>(Arrays.asList(paroleSplittate));
			   for(String s: set)
			   {
				   if(!stopWord.contains(s))
				   {
					   HashSet<String> sett = new HashSet<>();
					   sett.add(f.getName());
					   listaTesti.putIfAbsent(s, sett);
					   if(!listaTesti.get(s).contains(f.getName()))
					   {
						   HashSet<String> sett2 = listaTesti.get(s);;
						   sett2.addAll(sett);
						   listaTesti.replace(s,sett2);
					   }
				   }
			   }
		   }
		   catch(IOException e){}
	   }
	}
	/**
	 * Il metodo statico stopWords legge il file di testo "stopword" e inserisce all'interno dell'insme stopWords le parole contenute nel file.
	 * Le parole della stopWord serviranno per la creazione del Vocabolario.
	 */
	public static void stopWords()
	{
		
		File file = new File("src/stopword.txt");
		try(BufferedReader br = new BufferedReader(new FileReader(file)))
		{
			while(br.ready())
			{
				String[] linea = br.readLine().split("\t");
				for(String word : linea) stopWord.add(word);
			}
		}
		catch (IOException e) {System.out.println(e.getMessage());}
	}
	public Map<String, HashSet<String>> getListaTesti(){return listaTesti;}
	public Map<String,String> getLemmi() {return lemmi;}
	public static Double getNumeroDocumenti() {return numeroDocumenti;}
}





