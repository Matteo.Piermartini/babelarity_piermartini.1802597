package it.uniroma1.lcl.babelarity;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Path;

/**
 * La classe Corpus Manage � la responsabile del parsing, caricamento e salvataggio dei documenti. 
 * @author user
 *
 */
public class CorpusManager implements Serializable
{
	private static final long serialVersionUID = 2561591832444510738L;
	private static CorpusManager istanzaCorpus;
	public static CorpusManager getInstance()
	  {
	    if (istanzaCorpus == null)
	    {
	      istanzaCorpus = new CorpusManager();
	    }

	    return istanzaCorpus;
	  }	
	/**
	 * parseDocument() restituisce una nuova istanza di Document parsando un file di testo di cui � fornito il percorso in input.
	 * @param path
	 * @return
	 * @throws IOException
	 */
	public Document parseDocument(Path path) throws IOException
	{
		Document d = new Document();
		int g = 0;
		try(BufferedReader br = new BufferedReader(new FileReader(path.toFile())))
		{
			while(br.ready())
			{
				String[] linea = br.readLine().split("\t");
				if (g == 0)	
					{
						d.title = linea[0];
						d.id = linea[1];
					}
				else {
				for (int i = 0; i < linea.length; i++) d.content.add(linea[i]);
				}
				g++;
			}
		}
		catch (IOException e) {System.out.println(e.getMessage());}
		return d;
	}
	/**
	 * Salva su disco (nel file documents.txt) l�oggetto Document passato in input.
	 * @param d
	 * @throws IOException
	 */
	public void saveDocument(Document d) throws IOException 
	{
		FileOutputStream fos = new FileOutputStream("resources/documents.txt");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(d);
		oos.close();
	}
	/**
	 * loadDocument() carica da disco (dal file doucments.txt) l�oggetto Document identificato dal suo ID.
	 * @param id
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("resource")
	public Document loadDocument(String id) throws FileNotFoundException, IOException, ClassNotFoundException 
	{
		// read object from file
		FileInputStream fis = new FileInputStream("resources/documents.txt");
	    ObjectInputStream ois = new ObjectInputStream(fis);
		Document result = (Document) ois.readObject();
		if (id.equals(result.getId())) return result;
		ois.close();
		return null;
	}


}
