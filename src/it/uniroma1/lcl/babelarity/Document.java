package it.uniroma1.lcl.babelarity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * La classe Document implementa le caratteristiche di un documento.
 * @author user
 *
 */
public class Document implements LinguisticObject, Serializable
{
	private static final long serialVersionUID = 1L;
	public String title;
	public String id;
	public List<String> content = new ArrayList<>();
	public String getTitle() {return this.title;}
	public String getId() {return this.id;}
	public String getContent() 
	{
		String stringa = "";
		for(String s: this.content) stringa += s +" ";
		return stringa;
	}
	@Override
	public int hashCode(){return Objects.hashCode(id);}
	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Document c = (Document)o;
		return this.id.equals(c.id) ;
	}
	@Override
	public String getString() {return null;}
}
