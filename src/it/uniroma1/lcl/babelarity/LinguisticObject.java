package it.uniroma1.lcl.babelarity;


/**
 * LinguisticObject � l'interfaccia che permette di implemetare alle varie classi che implementano questa interfaccia
 * di usare il metodo getString() in modo diverso.
 * @author user
 *
 */
public interface LinguisticObject {String getString();}
