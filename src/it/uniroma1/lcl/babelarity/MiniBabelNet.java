package it.uniroma1.lcl.babelarity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * La classe MiniBabelNet rappresenta l�implementazione della omonima rete semantica ed espone un metodo per calcolare la similarit� tra oggetti 
 * linguistici ed implementa il pattern strategy per impostare le implementazioni di similarit� 
 * tra i diversi tipi di oggetti linguistici.
 * @author user
 *
 */
/**
 * @author user
 *
 */
public class MiniBabelNet implements Iterable<Synset>
{
	private static final String FILE_PATH_LEMMATIZATION ="resources/lemmatization-en.txt";
	private static final String FILE_PATH_DICTIONARY ="resources/dictionary.txt";
	private static final String FILE_PATH_GLOSSES = "resources/glosses.txt";
	private static final String FILE_PATH_RELATIONS = "resources/relations.txt";
	private static BabelSemanticSimilarityAvanzata b ;
	private static BabelLexicalSimilarity l;
	private static BabelDocumentSimilarityAvanzata d;
	/**
	 * mappaDizionario � una mappa che contiene come chiave l'id del synset e come valore un'insieme che conterr� 
	 * tutte le parole associate a quel synset.
	 */
	public static Map<String,Set<String>> mappaDizionario = leggiFileDizionary();
	public static Map<String,String> relazioniSynsets = new HashMap<>();
	/**
	 * lemmi � una mappa che contiene tutti i lemmi di tutte le parole che sono chiave.
	 */
	public static Map<String,String> lemmi = lemmatization();
	/**
	 * relazioni contiene tuti i synset che, posti come chiave della mappa, hanno altri synset che si 
	 * trovano in relazione con la chiave stessa, questi sono postiu come valore della mappa.
	 */
	public static Map<Synset, ArrayList<Synset>> relazioni = leggiFileRelazione();
	static MiniBabelNet istanza = null;
	public static MiniBabelNet getInstance() throws IOException {
		if (istanza == null) 
			{
				istanza = new MiniBabelNet();
				l = new BabelLexicalSimilarity();
				b = new BabelSemanticSimilarityAvanzata();
				d = new BabelDocumentSimilarityAvanzata();
			}
		return istanza;
	}
	/**
	 * leggiFileDizionary() legge il file dizionary.txt, il quale metodo restituisce una mappa
	 * contenente come chiave l'id del synset e come valore un'insieme che conterr� 
	 * tutte le parole associate a quel synset, cio� che si trovano sulla stessa linea
	 * dell'id del synset nel fle.
	 * @return
	 */
	private static Map<String, Set<String>> leggiFileDizionary() 
	{
		File file = new File(FILE_PATH_DICTIONARY);
		Map<String, Set<String>> mappa = new HashMap<>();
		try(BufferedReader br = new BufferedReader(new FileReader(file)))
		{
			while(br.ready())
			{
				String linea = br.readLine();
				String[] parole = linea.split("\t");
				Set<String> sett = new HashSet<>();
				for (int i = 1; i < parole.length; i++) {
					sett.add(parole[i]);
				}
				mappa.putIfAbsent(parole[0], sett);
				if(mappa.containsKey(parole[0]))
				{
					Set<String> sett1 = new HashSet<>();
					for (int i = 1; i < parole.length; i++) sett1.add(parole[i]);
					sett1.addAll(mappa.get(parole[0]));
				}	
			}
		}
		catch (IOException e) {System.out.println(e.getMessage());}
		return mappa;
	}
	/**
	 * getLemmi() restituisce la mappa dei lemmi creata con il metod lemmatization().
	 * @return
	 */
	public static Map<String,String> getLemmi(){return MiniBabelNet.lemmi;}	
	/**
	 * lemmatization() legge il file lemmatization-en.txt e restituisce una mappa contenente
	 * come chiave una stringa e come valore un'altra stringa cio� il lemma associato
	 * alla stringa chiave.
	 * @return
	 */
	public static Map<String,String> lemmatization()
	{
		Map<String,String> lemmi = new HashMap<>();
		File file = new File(FILE_PATH_LEMMATIZATION);
		try(BufferedReader br = new BufferedReader(new FileReader(file)))
		{
			while(br.ready())
			{
				String[] linea = br.readLine().split("\t");
				lemmi.putIfAbsent(linea[0], linea[1]);
				lemmi.putIfAbsent(linea[1], linea[1]);
			}
		}
		catch (IOException e) {System.out.println(e.getMessage());}
		return lemmi;
	}
	/**
	 * getSynset restituisce il synset relativo al'id specificato nella riga del documento che 
	 * si trova nella mappaDizionario creata da leggiFiledictionary().
	 * @param id
	 * @return
	 * @throws IOException
	 */
	public Synset getSynset(String id) throws IOException 
	{
		Synset s = new Synset();
		if (mappaDizionario.containsKey(id)) 
		{
			s.setId(id);
			for(String stringa : mappaDizionario.get(id))
				s.setDictionary(stringa);
		}
		else return null;
		return s;
	}
	/**
	 * getGlosses() legge il file glosses.txt e restituisce una lista di stringhe 
	 * che sono contenute all'interno del file in base all'id specificato.
	 * @param id
	 * @return
	 */
	public List<String> getGlosses(String id)
	{
		File file = new File(FILE_PATH_GLOSSES);
		List<String> lista = new ArrayList<>();
		try(BufferedReader br = new BufferedReader(new FileReader(file)))
		{
			while(br.ready())
			{
				String[] linea = br.readLine().split("\t");
				if (linea[0].equals(id))
				{
					for (int i = 1; i < linea.length; i++) {
						lista.add(linea[i]);
					}	
				}
			}
		}
		catch (IOException e) {System.out.println(e.getMessage());}
		return lista;
	}
	/**
	 * leggiFileRelazione() legge il file relations.txt e restituisce una mappa contentente 
	 * come chiave un Synset e come valore tutti i synset che si trovano in relazione con la 
	 * chiave stessa.
	 * @return
	 */
	public static Map<Synset, ArrayList<Synset>> leggiFileRelazione() // Crea la mappa per il semantic
	{
		Map<Synset, ArrayList<Synset>> mappa = new HashMap<>();
		try(BufferedReader br = new BufferedReader(new FileReader(new File(FILE_PATH_RELATIONS))))
		{
			while(br.ready())
			{
				String linea = br.readLine();
				String[] parole = linea.split("\t");
				Synset nuovoSynset = new Synset(parole[0]);
				relazioniSynsets.putIfAbsent(parole[0], parole[2]);
				ArrayList<Synset> listaDaAggiungere = new ArrayList<>();
				Synset s1 = new Synset(parole[1]);
				listaDaAggiungere.add(s1);
				mappa.putIfAbsent(nuovoSynset, listaDaAggiungere);
				if(mappa.containsKey(nuovoSynset))
					{
						ArrayList<Synset> lista = new ArrayList<>();
						lista.addAll(mappa.get(nuovoSynset));
						lista.addAll(listaDaAggiungere);
						mappa.replace(nuovoSynset, lista);
					}
			}
		}
		catch (IOException e) {System.out.println(e.getMessage());}
		return mappa;
	}
	/**
	 * getRelations resituisce una lista di tutti i Synset che sono n relazione con un synet indetificato
	 * con "id". Le strignhe delle lista conterranno l'id di arrivo con il tipo di relazione 
	 * che sta nell'indice 2 cio� terza posizione.
	 * @param id
	 * @return
	 * @throws IOException
	 */
	public  static List<String> getRelations(String id) throws IOException
	{
		Synset  s1 = new Synset(id);
		List<String>lista = new ArrayList<>();
		for(Synset  s: relazioni.get(s1)) 
			{
				String stringa = s.id + '_' + relazioniSynsets.get(id);
				lista.add(stringa);
			}
		return lista;
	}
	/**
	 * Restituisce le informazioni inerenti al Synset fornito in input sotto forma di stringa:
	 * ID\tPOS\tLEMMI\tGLOSSE\tRELAZIONI
	 * @param s
	 * @return
	 * @throws IOException
	 */
	public String getSynsetSummary(Synset s) throws IOException {
		String stringa = "";
		stringa += s.getId() + "\t";
		if (s.getId().endsWith("n")) stringa += "NOUN" +"\t";
		if (s.getId().endsWith("v")) stringa += "VERB" +"\t";
		if (s.getId().endsWith("a")) stringa += "ADJ" +"\t";
		if (s.getId().endsWith("r")) stringa += "ADV" +"\t";
		for (String str : s.dictionary) 
			stringa += str + ";";
		stringa += "\t";
		for(String str : getGlosses(s.getId()))
			stringa += str + ";";
		stringa += "\t";
		List<String> listaSynset = getRelations(s.getId());		
		for(String str:listaSynset.subList(0, listaSynset.size()-1) )stringa += str +';';
		return stringa;	
	}
	public Iterator<Synset> iterator() {return null;}
	/**
	 * getLemmas() restituisce uno o pi� lemmi associati alla parola flessa fornita in input.
	 * @param word
	 * @return
	 */
	public static List<String> getLemmas(String word) 
	{
		List<String> lista = new ArrayList<>();
		if(getLemmi().containsKey(word))
			{
				lista.add(getLemmi().get(word));
				return lista;
			}
		return new ArrayList<>();		
	}
	/**
	 * getSynsets() restituisce l�insieme di synset che contengono tra i loro sensi la parola in input.
	 * @param word
	 * @return
	 * @throws IOException
	 */
	public List<Synset> getSynsets(String word) throws IOException 
	{
		List<Synset> synsets = new ArrayList<>();
		for(String key : mappaDizionario.keySet())
		{
			if(mappaDizionario.get(key).contains(word))
			{
				Synset syn = new Synset();
				syn.id = key;
				List<String> lista = new ArrayList<>(mappaDizionario.get(key));
				syn.dictionary.addAll(lista);
				synsets.add(syn);
			}
		}
			return synsets;	
	}				
	/**
	 * Il computeSimilarity identifica di quale classe sono i due oggetti passati in input cosi da dovere 
	 * applicare un algoritmo diverso per il controllo della similarit�.
	 * @param o1
	 * @param o2
	 * @return
	 * @throws IOException
	 */
	public double computeSimilarity(LinguisticObject o1, LinguisticObject o2) throws IOException 
	{
		if (o1 instanceof Word) return l.computeSimilarity(o1, o2);
		if (o1 instanceof Synset) return b.lch(o1, o2);
		return  d.computeSimilarity(o1, o2);
	}
}





