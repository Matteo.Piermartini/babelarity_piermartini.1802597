package it.uniroma1.lcl.babelarity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * BabelNet contiene informazioni riguardo concetti ed entit� nominali per nomi, aggettivi, avverbi e verbi ed � 
 * costruita intorno al concetto di synset (synonym set o insieme di sinonimi). Un synset � un insieme di parole con la 
 * stessa parte del discorso (part-of-speech o POS) che possono essere usate e sostituite in un certo contesto.
 * @author user
 *
 */
public class Synset implements LinguisticObject, Comparable<Synset>
{
	public String id = "";
	public List<String> dictionary = new ArrayList<>();
	/**
	 * Il contatore viene posto a Integer.MAX_VALUE perch� poi nel calcolo della similarit� semantica
	 * proprio quando viene trovato il synset desiderato allora il contatore viene posto uguale a 0.
	 */
	public int contatore = Integer.MAX_VALUE;
	public Synset() {}
	public Synset(String id) {this.id = id;}
	public String getId() {return this.id;}
	public int getContatore() {return this.contatore;}
	public void setId(String id) {this.id = id;}
	public void setDictionary(String parola) {this.dictionary.add(parola);}
	public void setContatore(int contatore) {this.contatore = contatore;}
	@Override
	public String getString() {return null;}
	@Override
	public int hashCode(){return Objects.hashCode(id);}
	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Synset c = (Synset)o;
		return this.id.equals(c.id) ;
	}
	@Override
	public int compareTo(Synset arg0) { return id.compareTo(arg0.id);}
}
