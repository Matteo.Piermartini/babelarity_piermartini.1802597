package it.uniroma1.lcl.babelarity;

import java.util.Objects;

/**
 * Una Word � semplicemente una parola quindi � composta da una stringa.
 * Questa viene utilizzata nell'implemento della similarit� lessicale.
 * @author user
 *
 */
public class Word implements LinguisticObject
{
	public String stringa = "";
	public Word(String s) {this.stringa = s;}
	@Override
	public String getString() {return this.stringa;}
	@Override
	public int hashCode() {return Objects.hashCode(stringa);}
	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Word c = (Word)o;
		return this.stringa.equals(c.stringa) ;
	}
	/**
	 * fromString restituisce un nuovo oggetto Word data una determinata stringa.
	 * @param stringa
	 * @return
	 */
	public static Word fromString(String stringa){return new Word(stringa);	}	
	public String getStringa() {return this.stringa;}
	public void setStringa(String stringa) {this.stringa = stringa;}
}
